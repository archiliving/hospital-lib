import { Drug, PatientsRegister, HealthStatus } from "./patientsRegister";
import { Disease } from "./types/quarantine";
export declare const diseases: Disease[];
export declare const sideEffects: {
  requirement: HealthStatus;
  drugs: Drug[];
  effect: HealthStatus;
}[];
export declare class Quarantine {
  private _patientsHealthStatus;
  private _initialPatientsHealthStatus;
  private _drugs;
  constructor(patients: PatientsRegister);
  setDrugs(drugs: Drug[]): void;
  readonly drugs: Drug[];
  readonly initialPatientsHealthStatus: PatientsRegister;
  readonly patientsHealthStatus: PatientsRegister;
  wait40Days(): void;
  report(): PatientsRegister;
}
