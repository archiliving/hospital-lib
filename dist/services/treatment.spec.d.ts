import { Drug, HealthStatus } from "../patientsRegister";
import { SideEffect } from "../types/quarantine";
export declare class TreatmentUtilsTest {
    private _quarantine;
    private _diseases;
    setup(): void;
    checkApplyHealthStatusToEveryPatient(): void;
    checkSideEffectsProduceWithDrugs(drugs: Drug[], healthStatus: HealthStatus): void;
    checkDrugsWhichWillCureDisease(diseaseName: string, drugs: Drug[]): void;
    checkDrugsWhichWillPreventToDie(diseaseName: string, drugs: Drug[]): void;
    checkDrugsInformations(diseaseName: string, drugs: Drug[], result: {
        drugsWhichWillCure: Drug[];
        drugsWhichPreventToDie: Drug[];
        sideEffects: SideEffect[];
    }): void;
}
