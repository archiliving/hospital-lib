import { HealthStatus } from "../patientsRegister";
import { Drug } from "../patientsRegister";
import { Quarantine } from "../quarantine";
import { Disease, SideEffect } from "../types/quarantine";
export declare const applyHealthStatusToEveryPatient: (quarantine: Quarantine, healthStatusToApply: HealthStatus) => void;
export declare const sideEffectsProduceWithDrugs: (drugs: Drug[]) => SideEffect[];
export declare const drugsWhichWillCureDisease: (disease: Disease, drugs: Drug[]) => Drug[];
export declare const drugsWhichWillPreventToDie: (disease: Disease, drugs: Drug[]) => Drug[];
export declare const drugsInformations: (disease: Disease, drugs: Drug[]) => {
    drugsWhichWillCure: Drug[];
    drugsWhichPreventToDie: Drug[];
    sideEffects: SideEffect[];
};
