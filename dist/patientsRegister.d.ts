export declare type PatientsRegister = {
    [key: string]: number;
};
export declare enum Drug {
    ASPIRIN = "AS",
    ANTIBIOTIC = "AN",
    INSULIN = "I",
    PARACETAMOL = "P"
}
export declare enum HealthStatus {
    FEVER = "F",
    HEALTHY = "H",
    DIABETES = "D",
    TUBERCULOSIS = "T",
    DEAD = "X"
}
