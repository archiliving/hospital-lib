import { Drug, HealthStatus } from "../patientsRegister";
import { Quarantine } from "../quarantine";
import { Disease } from "../types/quarantine";

export const quarantine = new Quarantine({
  F: 1,
  H: 2,
  D: 3,
  T: 1,
  X: 0,
});

export const diseases: Disease[] = [
  {
    name: "Fever",
    code: HealthStatus.FEVER,
    cures: [Drug.ASPIRIN, Drug.PARACETAMOL],
    fatal: false,
  },
  {
    name: "Diabetes",
    code: HealthStatus.DIABETES,
    prevents_to_die: [Drug.INSULIN],
    fatal: true,
  },
  {
    name: "Tuberculosis",
    code: HealthStatus.TUBERCULOSIS,
    cures: [Drug.ANTIBIOTIC],
    fatal: false,
  },
];
