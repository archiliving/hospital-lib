export type PatientsRegister = {
  [key: string]: number;
};

export enum Drug {
  ASPIRIN = "AS",
  ANTIBIOTIC = "AN",
  INSULIN = "I",
  PARACETAMOL = "P",
}

export enum HealthStatus {
  FEVER = "F",
  HEALTHY = "H",
  DIABETES = "D",
  TUBERCULOSIS = "T",
  DEAD = "X",
}
