import { Drug, HealthStatus } from "../patientsRegister";

export interface Disease {
  name: string;
  code: HealthStatus;
  cures?: Drug[];
  prevents_to_die?: Drug[];
  fatal: boolean;
}

export interface SideEffect {
  requirement: HealthStatus | null;
  drugs: Drug[];
  effect: HealthStatus;
}
