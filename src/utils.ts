export const existsInArray = (valuesToCheck: string[], array: string[]) => {
  const set = new Set(array);

  return valuesToCheck.every((value) => {
    return set.has(value);
  });
};

export const incrementKeyValueIfExist = (
  value: number,
  object: any,
  key: string
): void => {
  if (value > 0) object[key] = key in object ? object[key] + value : value;
};
