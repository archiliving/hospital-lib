import { Expect, Setup, Test, TestCase, TestFixture } from "alsatian";
import { Quarantine } from "./quarantine";
import { Drug, PatientsRegister } from "./patientsRegister";

@TestFixture()
export class QuarantineTest {
  private quarantine: Quarantine;

  @Setup
  public setup() {
    // The responsibility of the Quarantine object is to simulate diseases on a group of patients.
    // It is initialized with a list of patients' health status, separated by a comma.
    // Each health status is described by one or more characters
    // (in the test below, we will always have only one disease / patient)
    // The characters mean:
    // H : Healthy
    // F : Fever
    // D : Diabetes
    // T : Tuberculosis

    ///i\ Use fixtures instead
    this.quarantine = new Quarantine({
      F: 1,
      H: 2,
      D: 3,
      T: 1,
      X: 0,
    });
    // Quarantine provides medicines to the patients, but can not target a specific group of patient.
    // The same medicines are always given to all the patients.

    // Then Quarantine can provide a report that gives the number of patients that have the given disease.
    // X means Dead
  }

  @Test()
  public beforeTreatment(): void {
    // diabetics die without insulin
    Expect(this.quarantine.report()).toEqual({
      F: 1,
      H: 2,
      D: 3,
      T: 1,
      X: 0,
    });
  }

  @Test()
  public noTreatment(): void {
    this.quarantine.wait40Days();
    // diabetics die without insulin
    Expect(this.quarantine.report()).toEqual({
      F: 1,
      H: 2,
      D: 0,
      T: 1,
      X: 3,
    });
  }
  @Test()
  public aspirin(): void {
    this.quarantine.setDrugs([Drug.ASPIRIN]);
    this.quarantine.wait40Days();
    // aspirin cure Fever
    Expect(this.quarantine.report()).toEqual({
      F: 0,
      H: 3,
      D: 0,
      T: 1,
      X: 3,
    });
  }

  @Test()
  public antibiotic(): void {
    this.quarantine.setDrugs([Drug.ANTIBIOTIC]);
    this.quarantine.wait40Days();
    // antibiotic cure Tuberculosis
    // but healthy people catch Fever if mixed with insulin.
    Expect(this.quarantine.report()).toEqual({
      F: 1,
      H: 3,
      D: 0,
      T: 0,
      X: 3,
    });
  }

  @Test()
  public insulin(): void {
    this.quarantine.setDrugs([Drug.INSULIN]);
    this.quarantine.wait40Days();
    // insulin prevent diabetic subject from dying, does not cure Diabetes,
    Expect(this.quarantine.report()).toEqual({
      F: 1,
      H: 2,
      D: 3,
      T: 1,
      X: 0,
    });
  }

  @Test()
  public antibioticPlusInsulin(): void {
    this.quarantine.setDrugs([Drug.ANTIBIOTIC, Drug.INSULIN]);
    this.quarantine.wait40Days();
    // if insulin is mixed with antibiotic, healthy people catch Fever
    Expect(this.quarantine.report()).toEqual({
      F: 3,
      H: 1,
      D: 3,
      T: 0,
      X: 0,
    });
  }

  @Test()
  public paracetamol(): void {
    this.quarantine.setDrugs([Drug.PARACETAMOL]);
    this.quarantine.wait40Days();
    // paracetamol heals fever
    Expect(this.quarantine.report()).toEqual({
      F: 0,
      H: 3,
      D: 0,
      T: 1,
      X: 3,
    });
  }

  @Test()
  public paracetamolAndAspirin(): void {
    this.quarantine.setDrugs([Drug.PARACETAMOL, Drug.ASPIRIN]);
    this.quarantine.wait40Days();
    // paracetamol kills subject if mixed with aspirin
    Expect(this.quarantine.report()).toEqual({
      F: 0,
      H: 0,
      D: 0,
      T: 0,
      X: 7,
    });
  }

  @Test()
  @TestCase(
    {
      F: 1,
      H: 2,
      D: 3,
      T: 1,
      X: 0,
    },
    [Drug.ANTIBIOTIC, Drug.ASPIRIN, Drug.PARACETAMOL],
    {
      F: 0,
      H: 0,
      D: 0,
      T: 0,
      X: 7,
    }
  )
  @TestCase(
    { F: 1, X: 2, D: 1 },
    [Drug.ANTIBIOTIC, Drug.ASPIRIN, Drug.PARACETAMOL],
    { F: 0, X: 4, D: 0 }
  )
  public paracetamolAndAspirinAndAntibiotic(
    patientsHealthStatus: PatientsRegister,
    drugs: Drug[],
    result: PatientsRegister
  ): void {
    const quarantine = new Quarantine(patientsHealthStatus);
    quarantine.setDrugs(drugs);
    quarantine.wait40Days();
    // paracetamol kills subject if mixed with aspirin and aspirin has no effect unfortunatly
    Expect(quarantine.report()).toEqual(result);
  }

  @Test()
  public shouldAddDeadStatusToReport(): void {
    const quarantine = new Quarantine({
      F: 1,
      H: 2,
      D: 3,
    });
    quarantine.setDrugs([Drug.PARACETAMOL]);
    quarantine.wait40Days();

    Expect(quarantine.report()).toEqual({
      F: 0,
      H: 3,
      D: 0,
      X: 3,
    });
  }

  @Test()
  public shouldAddHealthStatusToReport(): void {
    const quarantine = new Quarantine({
      F: 1,
      D: 3,
    });
    quarantine.setDrugs([Drug.ASPIRIN]);
    quarantine.wait40Days();

    Expect(quarantine.report()).toEqual({
      F: 0,
      H: 1,
      D: 0,
      X: 3,
    });
  }
}
