import { Expect, Setup, Test, TestCase, TestFixture } from "alsatian";

import { Quarantine } from "../quarantine";
import { Drug, HealthStatus } from "../patientsRegister";
import { Disease, SideEffect } from "../types/quarantine";
import {
  applyHealthStatusToEveryPatient,
  drugsInformations,
  drugsWhichWillCureDisease,
  drugsWhichWillPreventToDie,
  sideEffectsProduceWithDrugs,
} from "./treatment";
import { diseases, quarantine } from "../tests/fixtures";

@TestFixture()
export class TreatmentUtilsTest {
  public _quarantine: Quarantine;
  private _diseases: Disease[];

  @Setup
  public setup() {
    this._quarantine = quarantine;
    this._diseases = diseases;
  }

  @Test("Apply fever status to every patient")
  public checkApplyFeverStatusToEveryPatient(): void {
    this._quarantine.patientsHealthStatus = applyHealthStatusToEveryPatient(
      this._quarantine.patientsHealthStatus,
      HealthStatus.FEVER
    );
    Expect(this._quarantine.patientsHealthStatus).toEqual({
      F: 7,
      H: 0,
      D: 0,
      T: 0,
      X: 0,
    });
  }

  @Test("Get side effects of drugs")
  @TestCase([Drug.INSULIN, Drug.ANTIBIOTIC], [HealthStatus.FEVER])
  @TestCase([Drug.ASPIRIN, Drug.PARACETAMOL], [HealthStatus.DEAD])
  @TestCase(
    [Drug.INSULIN, Drug.ANTIBIOTIC, Drug.ASPIRIN, Drug.PARACETAMOL],
    [HealthStatus.FEVER, HealthStatus.DEAD]
  )
  @TestCase([Drug.ANTIBIOTIC], [])
  public checkSideEffectsProduceWithDrugs(
    drugs: Drug[],
    healthStatus: HealthStatus
  ): void {
    const sideEffects: SideEffect[] = sideEffectsProduceWithDrugs(drugs);
    Expect(sideEffects.map((sideEffect) => sideEffect.effect)).toEqual(
      healthStatus
    );
  }

  @Test("Get drugs which will cure disease")
  @TestCase("Fever", [Drug.ASPIRIN, Drug.PARACETAMOL])
  @TestCase("Diabetes", [])
  @TestCase("Tuberculosis", [Drug.ANTIBIOTIC])
  public checkDrugsWhichWillCureDisease(diseaseName: string, drugs: Drug[]) {
    const disease: Disease = this._diseases.find(
      (disease) => disease.name === diseaseName
    );
    const result: Drug[] = drugsWhichWillCureDisease(disease, drugs);
    Expect(result).toEqual(drugs);
  }

  @Test("Get drugs which will prevent to die")
  @TestCase("Diabetes", [Drug.INSULIN])
  @TestCase("Fever", [])
  public checkDrugsWhichWillPreventToDie(diseaseName: string, drugs: Drug[]) {
    const disease: Disease = this._diseases.find(
      (disease) => disease.name === diseaseName
    );
    const result: Drug[] = drugsWhichWillPreventToDie(disease, drugs);
    Expect(result).toEqual(drugs);
  }

  @Test("Get drugs informations")
  @TestCase("Fever", [Drug.ASPIRIN, Drug.PARACETAMOL], {
    drugsWhichWillCure: [Drug.ASPIRIN, Drug.PARACETAMOL],
    drugsWhichPreventToDie: [],
    sideEffects: [
      {
        requirement: null,
        drugs: [Drug.ASPIRIN, Drug.PARACETAMOL],
        effect: HealthStatus.DEAD,
      },
    ],
  })
  @TestCase("Diabetes", [Drug.INSULIN], {
    drugsWhichWillCure: [],
    drugsWhichPreventToDie: [Drug.INSULIN],
    sideEffects: [],
  })
  @TestCase("Tuberculosis", [Drug.ANTIBIOTIC], {
    drugsWhichWillCure: [Drug.ANTIBIOTIC],
    drugsWhichPreventToDie: [],
    sideEffects: [],
  })
  public checkDrugsInformations(
    diseaseName: string,
    drugs: Drug[],
    result: {
      drugsWhichWillCure: Drug[];
      drugsWhichPreventToDie: Drug[];
      sideEffects: SideEffect[];
    }
  ) {
    const disease: Disease = this._diseases.find(
      (disease) => disease.name === diseaseName
    );
    const drugsInformationsResult: any = drugsInformations(disease, drugs);
    Expect(drugsInformationsResult).toEqual(result);
  }
}
