import { HealthStatus, PatientsRegister } from "../patientsRegister";
import { Drug } from "../patientsRegister";
import { Quarantine, sideEffects } from "../quarantine";
import { Disease, SideEffect } from "../types/quarantine";
import { incrementKeyValueIfExist } from "../utils";

export const applyHealthStatusToEveryPatient = (
  patientsHealthStatus: PatientsRegister,
  healthStatusToApply: HealthStatus
) => {
  const patientsStatus = { ...patientsHealthStatus };
  Object.keys(patientsStatus).forEach((healthStatus) => {
    if (healthStatus !== healthStatusToApply) {
      incrementKeyValueIfExist(
        patientsStatus[healthStatus],
        patientsStatus,
        healthStatusToApply
      );
      patientsStatus[healthStatus] = 0;
    }
  });

  return patientsStatus;
};

export const sideEffectsProduceWithDrugs = (drugs: Drug[]): SideEffect[] => {
  let currentSideEffect: SideEffect[] = [];
  sideEffects.forEach((sideEffect: SideEffect) => {
    let allDrugsAreInSideEffect = sideEffect.drugs.every((sideEffectDrug) => {
      return drugs.indexOf(sideEffectDrug) !== -1;
    });

    if (allDrugsAreInSideEffect) {
      currentSideEffect.push(sideEffect);
    }
  });
  return currentSideEffect;
};

export const drugsWhichWillCureDisease = (
  disease: Disease,
  drugs: Drug[]
): Drug[] => {
  return disease.cures !== undefined
    ? disease.cures.filter((drug) => {
        return drugs.indexOf(drug) !== -1;
      })
    : [];
};

export const drugsWhichWillPreventToDie = (
  disease: Disease,
  drugs: Drug[]
): Drug[] => {
  return disease.prevents_to_die !== undefined
    ? disease.prevents_to_die.filter((drug) => {
        return drugs.indexOf(drug) !== -1;
      })
    : [];
};

export const drugsInformations = (
  disease: Disease,
  drugs: Drug[]
): {
  drugsWhichWillCure: Drug[];
  drugsWhichPreventToDie: Drug[];
  sideEffects: SideEffect[];
} => {
  const drugsWhichWillCure = drugsWhichWillCureDisease(disease, drugs);

  const drugsWhichPreventToDie = drugsWhichWillPreventToDie(disease, drugs);

  const sideEffects: SideEffect[] = sideEffectsProduceWithDrugs(drugs);

  return { drugsWhichWillCure, drugsWhichPreventToDie, sideEffects };
};

export const sortSideEffectsByEffect = (
  sideEffectsToSort: SideEffect[],
  healthStatus: HealthStatus
): SideEffect[] => {
  let sortedSideEffects: SideEffect[] = [];
  sideEffectsToSort.forEach((sideEffect: SideEffect) => {
    if (sideEffect.effect === healthStatus) {
      sortedSideEffects.unshift(sideEffect);
    } else {
      sortedSideEffects.push(sideEffect);
    }
  });
  return sortedSideEffects;
};

export const isThereDrugsToCureFatality = (
  drugs: Drug[],
  disease: Disease
): boolean => {
  const { drugsWhichWillCure, drugsWhichPreventToDie } = drugsInformations(
    disease,
    drugs
  );

  return (
    drugsWhichWillCure.length > 0 ||
    drugsWhichPreventToDie.length > 0 ||
    !disease.fatal
  );
};

export const isItPossibleToChangeHealthStatus = (
  quarantine: Quarantine,
  healthStatus: HealthStatus
): boolean => {
  const healthStatusUpdated: HealthStatus[] = quarantine.healthStatusUpdated;
  const initialPatientsHealthStatus: PatientsRegister =
    quarantine.initialPatientsHealthStatus;

  let numberOfHealthStatusUpdated = 0;
  healthStatusUpdated.forEach((status) => {
    if (status === healthStatus) numberOfHealthStatusUpdated++;
  });

  return (
    numberOfHealthStatusUpdated < initialPatientsHealthStatus[healthStatus]
  );
};
