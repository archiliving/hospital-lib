import { Drug, PatientsRegister, HealthStatus } from "./patientsRegister";
import {
  applyHealthStatusToEveryPatient,
  drugsInformations,
  isItPossibleToChangeHealthStatus,
  isThereDrugsToCureFatality,
  sideEffectsProduceWithDrugs,
  sortSideEffectsByEffect,
} from "./services/treatment";
import { Disease, SideEffect } from "./types/quarantine";
import { incrementKeyValueIfExist } from "./utils";

export const diseases: Disease[] = [
  {
    name: "Fever",
    code: HealthStatus.FEVER,
    cures: [Drug.ASPIRIN, Drug.PARACETAMOL],
    fatal: false,
  },
  {
    name: "Diabetes",
    code: HealthStatus.DIABETES,
    prevents_to_die: [Drug.INSULIN],
    fatal: true,
  },
  {
    name: "Tuberculosis",
    code: HealthStatus.TUBERCULOSIS,
    cures: [Drug.ANTIBIOTIC],
    fatal: false,
  },
];

export const sideEffects = [
  {
    requirement: HealthStatus.HEALTHY,
    drugs: [Drug.INSULIN, Drug.ANTIBIOTIC],
    effect: HealthStatus.FEVER,
  },
  {
    requirement: null,
    drugs: [Drug.ASPIRIN, Drug.PARACETAMOL],
    effect: HealthStatus.DEAD,
  },
];

export class Quarantine {
  private _patientsHealthStatus: PatientsRegister;
  private _initialPatientsHealthStatus: PatientsRegister;
  private _drugs: Drug[] = [];
  private _healthStatusUpdated: HealthStatus[] = [];

  constructor(patients: PatientsRegister) {
    this._initialPatientsHealthStatus = {
      ...patients,
    };
    this._patientsHealthStatus = {
      ...patients,
    };
  }

  public setDrugs(drugs: Drug[]) {
    this._drugs = drugs;
  }

  get healthStatusUpdated(): HealthStatus[] {
    return this._healthStatusUpdated;
  }

  get drugs(): Drug[] {
    return this._drugs;
  }

  get initialPatientsHealthStatus(): PatientsRegister {
    return this._initialPatientsHealthStatus;
  }

  get patientsHealthStatus(): PatientsRegister {
    return this._patientsHealthStatus;
  }

  set patientsHealthStatus(patientsHealthStatus) {
    this._patientsHealthStatus = patientsHealthStatus;
  }

  public applySideEffects(): void {
    const sideEffectsSortedByPriority = sortSideEffectsByEffect(
      sideEffectsProduceWithDrugs(this._drugs),
      HealthStatus.DEAD
    );

    sideEffectsSortedByPriority.every((sideEffect: SideEffect) => {
      if (sideEffect.requirement !== null) {
        if (isItPossibleToChangeHealthStatus(this, sideEffect.effect)) {
          incrementKeyValueIfExist(
            this._patientsHealthStatus[sideEffect.requirement],
            this._patientsHealthStatus,
            sideEffect.effect
          );
          this._patientsHealthStatus[sideEffect.requirement] = 0;
        }
        return true;
      } else {
        this._patientsHealthStatus = applyHealthStatusToEveryPatient(
          this._patientsHealthStatus,
          sideEffect.effect
        );
        return false;
      }
    });
  }

  public wait40Days(): void {
    this.applySideEffects();

    let newPatientsHealthStatus: PatientsRegister = {
      ...this._patientsHealthStatus,
    };

    Object.keys(newPatientsHealthStatus).every((healthStatus: HealthStatus) => {
      if (newPatientsHealthStatus[healthStatus] > 0) {
        const disease = diseases.find(
          (disease: Disease) => disease.code === healthStatus
        );

        if (disease) {
          const { drugsWhichWillCure } = drugsInformations(disease, this.drugs);

          if (!isThereDrugsToCureFatality(this._drugs, disease)) {
            if (isItPossibleToChangeHealthStatus(this, healthStatus)) {
              incrementKeyValueIfExist(
                newPatientsHealthStatus[healthStatus],
                newPatientsHealthStatus,
                HealthStatus.DEAD
              );

              if (healthStatus in newPatientsHealthStatus) {
                newPatientsHealthStatus[healthStatus] = 0;
              }
            }
          } else if (drugsWhichWillCure.length > 0) {
            if (isItPossibleToChangeHealthStatus(this, healthStatus)) {
              incrementKeyValueIfExist(
                newPatientsHealthStatus[healthStatus],
                newPatientsHealthStatus,
                HealthStatus.HEALTHY
              );

              if (healthStatus in newPatientsHealthStatus) {
                newPatientsHealthStatus[healthStatus] = 0;
              }
            }
          }
        }
      }

      return true;
    });

    this._patientsHealthStatus = newPatientsHealthStatus;
  }

  public report(): PatientsRegister {
    return this._patientsHealthStatus;
  }
}
